﻿using System;

namespace ConsoleGame
{
    internal class Start
    {

        public static void StartGame()
        {
            Renderer renderer = Renderer.Instance;
            renderer.DrawCanvas();
            renderer.Start();
        }
        public static void Main(string[] args)
        {
            Console.Clear();

            Renderer renderer = Renderer.Instance;

            // Пример добавление объекта на сцену
            Board board = new Board();
            renderer.Scene.Add(board);
            StartGame();
        }
    }
}


